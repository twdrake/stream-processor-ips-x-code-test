# Stream Processor - IPS-X Code Test

A stream processing service implemented using the Akka-stream library.
## How to run
### Prerequisites
* Java 8
* Apache Maven 3.x
### Commands
Having cloned the project, executing the following maven command from the project's root directory should compile, test and package the source code and then run in the terminal an example application that demonstrates the
streaming processor.
```
mvn verify
```
The example application reads a JSON encoded `Transaction` object from an input file (`example-data/testinput`), processes the
transaction with a randomly generated result then and outputs the resulting `TransactionResult` object as a JSON encoded string to Stdout.
### Enabling annotation processing in your IDE
This project makes use of the `immutables` annotation processor and as result a small amount of configuration may be required
for the generated sources to be resolved in your IDE. For Intellij IDEA this involves:
1. Using the dialog `Preferences > Project Settings > Compiler > Annotation Processors` and enabling annotation processing there.
2. Ensuring annotation processors are obtained from the project classpath
3. Specify sources directory as `target/generated-sources/annotations` and `target/generated-test-sources/annotations`

NB: You _may_ need to re-run `mvn clean` and `mvn compile` in order for IDEA to pick up the source directories following the settings changes above.

More information [here](https://immutables.github.io/apt.html)