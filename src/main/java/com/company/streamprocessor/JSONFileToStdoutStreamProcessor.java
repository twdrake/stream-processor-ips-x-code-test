package com.company.streamprocessor;

import akka.Done;
import akka.stream.IOResult;
import akka.stream.javadsl.*;
import akka.util.ByteString;
import com.company.Service;
import com.company.models.Transaction;
import com.company.models.TransactionResult;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.util.concurrent.CompletionStage;

public class JSONFileToStdoutStreamProcessor extends AbstractTransactionStreamProcessor<CompletionStage<IOResult>, CompletionStage<Done>> {

    private static final int MAX_FRAMING_LENGTH = 500;

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private File sourceFile;

    public JSONFileToStdoutStreamProcessor(Service service, File sourceFile) {
        super(service);
        this.sourceFile = sourceFile;
    }

    @Override
    protected Source<Transaction, CompletionStage<IOResult>> source() {
        return FileIO.fromFile(sourceFile)
                .via(Framing.delimiter(ByteString.fromString("\n"), MAX_FRAMING_LENGTH, FramingTruncation.ALLOW))
                .map(ByteString::utf8String)
                .map(s -> objectMapper.readValue(s, Transaction.class));
    }

    @Override
    protected Sink<TransactionResult, CompletionStage<Done>> sink() {
        return Sink.foreach(t -> System.out.println(objectMapper.writeValueAsString(t)));
    }
}
