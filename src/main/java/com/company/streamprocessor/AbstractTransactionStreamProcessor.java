package com.company.streamprocessor;

import akka.actor.ActorSystem;
import akka.japi.Pair;
import akka.stream.KillSwitch;
import akka.stream.KillSwitches;
import akka.stream.UniqueKillSwitch;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.company.Service;
import com.company.models.FailedTransactionResult;
import com.company.models.SuccessfulTransactionResult;
import com.company.models.Transaction;
import com.company.models.TransactionResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Optional;

public abstract class AbstractTransactionStreamProcessor<T, U> implements TransactionStreamProcessor<T, U> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractTransactionStreamProcessor.class);

    private static final int THROTTLE_MAX_ELEMENTS = 1;

    private static final int THROTTLE_DURATION_MILLIS = 1000;

    private static final int SERVICE_CALL_PARALLELISM = 10;

    private KillSwitch killSwitch;

    private Service service;

    private ActorSystem actorSystem;

    public AbstractTransactionStreamProcessor(Service service) {
        this.service = service;
        this.actorSystem = ActorSystem.create("TransactionStreamProcessor");
    }

    @Override
    public Pair<T, U> start() {
        LOG.info("Starting stream processor using actor system: {}", actorSystem);
        Pair<Pair<T, UniqueKillSwitch>, U> matValues =
                source().throttle(THROTTLE_MAX_ELEMENTS, Duration.ofMillis(THROTTLE_DURATION_MILLIS))
                        .viaMat(KillSwitches.single(), Keep.both())
                        .mapAsync(SERVICE_CALL_PARALLELISM, t ->
                                service.processTransaction(t).handle((v, ex) -> new Pair<>(t, ex != null ? Optional.of(ex.getMessage()) : v))
                        )
                        .map(p -> {
                            Transaction tx = p.first();
                            Optional<String> result = p.second();
                            if (result.isPresent()) {
                                return FailedTransactionResult.builder().iD(tx.getID()).errorMessage(result.get()).build();
                            } else {
                                return SuccessfulTransactionResult.builder().iD(tx.getID()).build();
                            }
                        })
                        .toMat(sink(), Keep.both())
                        .run(actorSystem);
        killSwitch = matValues.first().second();
        return new Pair<>(matValues.first().first(), matValues.second());
    }

    @Override
    public void stop() {
        LOG.info("Stopping stream processor");
        killSwitch.shutdown();
    }

    protected abstract Source<Transaction, T> source();

    protected abstract Sink<TransactionResult, U> sink();
}
