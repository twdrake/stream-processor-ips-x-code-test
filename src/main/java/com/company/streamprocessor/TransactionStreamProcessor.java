package com.company.streamprocessor;

import akka.japi.Pair;

public interface TransactionStreamProcessor<T, U> {

    Pair<T, U> start();

    void stop();
}
