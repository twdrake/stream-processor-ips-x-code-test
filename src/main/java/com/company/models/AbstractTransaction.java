package com.company.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.UUID;

@Value.Immutable
@JsonDeserialize(as = Transaction.class)
public abstract class AbstractTransaction implements Message {
    @Override
    @Value.Default
    @JsonProperty(value = "id")
    public UUID getID() {
        return UUID.randomUUID();
    }

    public abstract String getMessage();
}
