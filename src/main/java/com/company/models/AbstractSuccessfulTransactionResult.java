package com.company.models;

import org.immutables.value.Value;

@Value.Immutable
public abstract class AbstractSuccessfulTransactionResult extends TransactionResult {
}
