package com.company.models;

import java.util.UUID;

public abstract class TransactionResult implements Message {
    @Override
    public abstract UUID getID();
}
