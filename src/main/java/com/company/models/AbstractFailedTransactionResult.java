package com.company.models;

import org.immutables.value.Value;

@Value.Immutable
public abstract class AbstractFailedTransactionResult extends TransactionResult {

    public abstract String getErrorMessage();
}
