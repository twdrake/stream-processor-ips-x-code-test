package com.company;

import com.company.models.Transaction;

import java.util.Optional;
import java.util.concurrent.CompletionStage;

public interface Service {
    CompletionStage<Optional<String>> processTransaction(Transaction tx);
}