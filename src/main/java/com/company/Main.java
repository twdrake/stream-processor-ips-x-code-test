package com.company;

import com.company.streamprocessor.JSONFileToStdoutStreamProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CompletableFuture;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        try {
            Random random = new Random();
            File inputFile = new File(args[0]);
            Service simpleService = tx -> {
                CompletableFuture<Optional<String>> processResult;
                switch (random.nextInt(3)) {
                    case 0:
                        processResult = CompletableFuture.completedFuture(Optional.empty());
                        break;
                    case 1:
                        processResult = CompletableFuture.completedFuture(Optional.of("Randomly generated error"));
                        break;
                    default:
                        processResult = CompletableFuture.supplyAsync(() -> {
                            throw new RuntimeException("Randomly generated exception");
                        });
                }
                return processResult;
            };
            JSONFileToStdoutStreamProcessor processor = new JSONFileToStdoutStreamProcessor(simpleService, inputFile);
            Runtime.getRuntime().addShutdownHook(new Thread(processor::stop));
            processor.start().second().toCompletableFuture().get();
        } catch (Exception ex) {
            LOG.error("Exception thrown in main():", ex);
            System.exit(1);
        }
    }
}
