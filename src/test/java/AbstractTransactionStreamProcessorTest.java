import akka.japi.Pair;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.*;
import com.company.Service;
import com.company.models.FailedTransactionResult;
import com.company.models.SuccessfulTransactionResult;
import com.company.models.Transaction;
import com.company.models.TransactionResult;
import com.company.streamprocessor.AbstractTransactionStreamProcessor;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class AbstractTransactionStreamProcessorTest {

    private static final int PATIENCE = 5;

    @Test
    void singleSuccessfulTransaction() {
        // given
        Service mockService = Mockito.mock(Service.class);
        Pair<SourceQueueWithComplete<Transaction>, SinkQueueWithCancel<TransactionResult>> queues = startTestProcessor(mockService);
        SourceQueueWithComplete<Transaction> in = queues.first();
        SinkQueue<TransactionResult> out = queues.second();

        Transaction testTransaction = Transaction.builder().message("Test message").build();

        // when
        when(mockService.processTransaction(testTransaction)).thenReturn(CompletableFuture.completedFuture(Optional.empty()));

        in.offer(testTransaction);
        in.complete();

        // then
        try {
            Optional<TransactionResult> firstFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertTrue(firstFromStream.isPresent());
            assertTrue(firstFromStream.get() instanceof SuccessfulTransactionResult);
            assertEquals(firstFromStream.get().getID(), testTransaction.getID());

            Optional<TransactionResult> secondFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertFalse(secondFromStream.isPresent());
        } catch (Exception ex) {
            fail(ex);
        }
    }

    @Test
    void twoSuccessfulTransactions() {
        // given
        Service mockService = Mockito.mock(Service.class);
        Pair<SourceQueueWithComplete<Transaction>, SinkQueueWithCancel<TransactionResult>> queues = startTestProcessor(mockService);
        SourceQueueWithComplete<Transaction> in = queues.first();
        SinkQueue<TransactionResult> out = queues.second();

        Transaction testTransaction = Transaction.builder().message("Test message").build();
        Transaction testTransaction2 = Transaction.builder().message("Test message 2").build();

        // when
        when(mockService.processTransaction(testTransaction)).thenReturn(CompletableFuture.completedFuture(Optional.empty()));
        when(mockService.processTransaction(testTransaction2)).thenReturn(CompletableFuture.completedFuture(Optional.empty()));

        in.offer(testTransaction);
        in.offer(testTransaction2);
        in.complete();

        // then
        try {
            Optional<TransactionResult> firstFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertTrue(firstFromStream.isPresent());
            assertTrue(firstFromStream.get() instanceof SuccessfulTransactionResult);
            assertEquals(firstFromStream.get().getID(), testTransaction.getID());

            Optional<TransactionResult> secondFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertTrue(secondFromStream.isPresent());
            assertTrue(secondFromStream.get() instanceof SuccessfulTransactionResult);
            assertEquals(secondFromStream.get().getID(), testTransaction2.getID());

            Optional<TransactionResult> thirdFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertFalse(thirdFromStream.isPresent());
        } catch (Exception ex) {
            fail(ex);
        }
    }

    @Test
    void singleFailedTransaction() {
        // given
        Service mockService = Mockito.mock(Service.class);
        Pair<SourceQueueWithComplete<Transaction>, SinkQueueWithCancel<TransactionResult>> queues = startTestProcessor(mockService);
        SourceQueueWithComplete<Transaction> in = queues.first();
        SinkQueue<TransactionResult> out = queues.second();

        String failureMessage = "Test Failure Message";

        Transaction testTransaction = Transaction.builder().message("Test message").build();

        // when
        when(mockService.processTransaction(testTransaction)).thenReturn(CompletableFuture.completedFuture(Optional.of(failureMessage)));

        in.offer(testTransaction);
        in.complete();

        // then
        try {
            Optional<TransactionResult> firstFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertTrue(firstFromStream.isPresent());
            assertTrue(firstFromStream.get() instanceof FailedTransactionResult);
            assertEquals(firstFromStream.get().getID(), testTransaction.getID());
            assertEquals(((FailedTransactionResult) (firstFromStream.get())).getErrorMessage(), failureMessage);

            Optional<TransactionResult> secondFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertFalse(secondFromStream.isPresent());
        } catch (Exception ex) {
            fail(ex);
        }
    }

    @Test
    void twoFailedTransactions() {
        // given
        Service mockService = Mockito.mock(Service.class);
        Pair<SourceQueueWithComplete<Transaction>, SinkQueueWithCancel<TransactionResult>> queues = startTestProcessor(mockService);
        SourceQueueWithComplete<Transaction> in = queues.first();
        SinkQueue<TransactionResult> out = queues.second();

        String failureMessage = "Test Failure Message";
        String failureMessage2 = "Test Failure Message 2";

        Transaction testTransaction = Transaction.builder().message("Test message").build();
        Transaction testTransaction2 = Transaction.builder().message("Test message 2").build();

        // when
        when(mockService.processTransaction(testTransaction)).thenReturn(CompletableFuture.completedFuture(Optional.of(failureMessage)));
        when(mockService.processTransaction(testTransaction2)).thenReturn(CompletableFuture.completedFuture(Optional.of(failureMessage2)));

        in.offer(testTransaction);
        in.offer(testTransaction2);
        in.complete();

        // then
        try {
            Optional<TransactionResult> firstFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertTrue(firstFromStream.isPresent());
            assertTrue(firstFromStream.get() instanceof FailedTransactionResult);
            assertEquals(firstFromStream.get().getID(), testTransaction.getID());
            assertEquals(((FailedTransactionResult) (firstFromStream.get())).getErrorMessage(), failureMessage);

            Optional<TransactionResult> secondFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertTrue(secondFromStream.isPresent());
            assertTrue(secondFromStream.get() instanceof FailedTransactionResult);
            assertEquals(secondFromStream.get().getID(), testTransaction2.getID());
            assertEquals(((FailedTransactionResult) (secondFromStream.get())).getErrorMessage(), failureMessage2);

            Optional<TransactionResult> thirdFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertFalse(thirdFromStream.isPresent());
        } catch (Exception ex) {
            fail(ex);
        }
    }

    @Test
    void singleExceptionalTransactions() {
        // given
        Service mockService = Mockito.mock(Service.class);
        Pair<SourceQueueWithComplete<Transaction>, SinkQueueWithCancel<TransactionResult>> queues = startTestProcessor(mockService);
        SourceQueueWithComplete<Transaction> in = queues.first();
        SinkQueue<TransactionResult> out = queues.second();

        Transaction testTransaction = Transaction.builder().message("Test message").build();

        String exceptionMessage = "Test Exception Message";

        // when
        when(mockService.processTransaction(testTransaction)).thenReturn(CompletableFuture.supplyAsync(() -> {
            throw new RuntimeException(exceptionMessage);
        }));

        in.offer(testTransaction);
        in.complete();

        // then
        try {
            Optional<TransactionResult> firstFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertTrue(firstFromStream.isPresent());
            assertTrue(firstFromStream.get() instanceof FailedTransactionResult);
            assertEquals(firstFromStream.get().getID(), testTransaction.getID());
            assertEquals(((FailedTransactionResult) (firstFromStream.get())).getErrorMessage(),
                    String.format("%s: %s", RuntimeException.class.getName(), exceptionMessage)
            );

            Optional<TransactionResult> thirdFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertFalse(thirdFromStream.isPresent());
        } catch (Exception ex) {
            fail(ex);
        }
    }

    @Test
    void twoExceptionalTransaction() {
        // given
        Service mockService = Mockito.mock(Service.class);
        Pair<SourceQueueWithComplete<Transaction>, SinkQueueWithCancel<TransactionResult>> queues = startTestProcessor(mockService);
        SourceQueueWithComplete<Transaction> in = queues.first();
        SinkQueue<TransactionResult> out = queues.second();

        Transaction testTransaction = Transaction.builder().message("Test message").build();
        Transaction testTransaction2 = Transaction.builder().message("Test message 2").build();

        String exceptionMessage = "Test Exception Message";
        String exceptionMessage2 = "Test Exception Message 2";

        // when
        when(mockService.processTransaction(testTransaction)).thenReturn(CompletableFuture.supplyAsync(() -> {
            throw new RuntimeException(exceptionMessage);
        }));

        when(mockService.processTransaction(testTransaction2)).thenReturn(CompletableFuture.supplyAsync(() -> {
            throw new RuntimeException(exceptionMessage2);
        }));

        in.offer(testTransaction);
        in.offer(testTransaction2);
        in.complete();

        // then
        try {
            Optional<TransactionResult> firstFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertTrue(firstFromStream.isPresent());
            assertTrue(firstFromStream.get() instanceof FailedTransactionResult);
            assertEquals(firstFromStream.get().getID(), testTransaction.getID());
            assertEquals(((FailedTransactionResult) (firstFromStream.get())).getErrorMessage(),
                    String.format("%s: %s", RuntimeException.class.getName(), exceptionMessage)
            );

            Optional<TransactionResult> secondFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertTrue(secondFromStream.isPresent());
            assertTrue(secondFromStream.get() instanceof FailedTransactionResult);
            assertEquals(secondFromStream.get().getID(), testTransaction2.getID());
            assertEquals(((FailedTransactionResult) (secondFromStream.get())).getErrorMessage(),
                    String.format("%s: %s", RuntimeException.class.getName(), exceptionMessage2)
            );

            Optional<TransactionResult> thirdFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertFalse(thirdFromStream.isPresent());
        } catch (Exception ex) {
            fail(ex);
        }
    }

    @Test
    void threeDifferentTransactions() {
        // given
        Service mockService = Mockito.mock(Service.class);
        Pair<SourceQueueWithComplete<Transaction>, SinkQueueWithCancel<TransactionResult>> queues = startTestProcessor(mockService);
        SourceQueueWithComplete<Transaction> in = queues.first();
        SinkQueue<TransactionResult> out = queues.second();

        Transaction testTransaction = Transaction.builder().message("Test message").build();
        Transaction testTransaction2 = Transaction.builder().message("Test message 2").build();
        Transaction testTransaction3 = Transaction.builder().message("Test message 3").build();

        String failureMessage = "Test Failure Message";
        String exceptionMessage = "Test Exception Message";

        // when
        when(mockService.processTransaction(testTransaction)).thenReturn(CompletableFuture.completedFuture(Optional.empty()));
        when(mockService.processTransaction(testTransaction2)).thenReturn(CompletableFuture.completedFuture(Optional.of(failureMessage)));
        when(mockService.processTransaction(testTransaction3)).thenReturn(CompletableFuture.supplyAsync(() -> {
            throw new RuntimeException(exceptionMessage);
        }));

        in.offer(testTransaction);
        in.offer(testTransaction2);
        in.offer(testTransaction3);
        in.complete();

        // then
        try {
            Optional<TransactionResult> firstFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertTrue(firstFromStream.isPresent());
            assertTrue(firstFromStream.get() instanceof SuccessfulTransactionResult);
            assertEquals(firstFromStream.get().getID(), testTransaction.getID());

            Optional<TransactionResult> secondFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertTrue(secondFromStream.isPresent());
            assertTrue(secondFromStream.get() instanceof FailedTransactionResult);
            assertEquals(secondFromStream.get().getID(), testTransaction2.getID());
            assertEquals(((FailedTransactionResult) (secondFromStream.get())).getErrorMessage(), failureMessage);

            Optional<TransactionResult> thirdFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertTrue(thirdFromStream.isPresent());
            assertTrue(thirdFromStream.get() instanceof FailedTransactionResult);
            assertEquals(thirdFromStream.get().getID(), testTransaction3.getID());
            assertEquals(((FailedTransactionResult) (thirdFromStream.get())).getErrorMessage(),
                    String.format("%s: %s", RuntimeException.class.getName(), exceptionMessage)
            );

            Optional<TransactionResult> fourthFromStream = out.pull().toCompletableFuture().get(PATIENCE, TimeUnit.SECONDS);
            assertFalse(fourthFromStream.isPresent());
        } catch (Exception ex) {
            fail(ex);
        }
    }

    private Pair<SourceQueueWithComplete<Transaction>, SinkQueueWithCancel<TransactionResult>> startTestProcessor(Service service) {
        return new AbstractTransactionStreamProcessor<SourceQueueWithComplete<Transaction>, SinkQueueWithCancel<TransactionResult>>(service) {
            @Override
            protected Source<Transaction, SourceQueueWithComplete<Transaction>> source() {
                return Source.queue(10, OverflowStrategy.dropHead());
            }

            @Override
            protected Sink<TransactionResult, SinkQueueWithCancel<TransactionResult>> sink() {
                return Sink.queue();
            }
        }.start();
    }
}
